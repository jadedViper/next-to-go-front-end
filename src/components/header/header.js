import React, {Component} from 'react';

class Header extends Component {
    render() {
        return (
            <div className="site-header">
                <h2>Next To Go</h2>
            </div>
        );
    }
}

export default Header;
