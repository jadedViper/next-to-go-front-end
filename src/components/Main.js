require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';
import thunkMiddleware from 'redux-thunk';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import createLogger from 'redux-logger';
import {Router, Route, IndexRoute, useRouterHistory} from 'react-router';
import {createHistory} from 'history';
import Site from '../containers/layout/site';
import NextUpContainer from './nextup/nextupcontainer';
import appStore from '../store/appstore';

const loggerMiddleware = createLogger();
const store = createStore(
    appStore,
    applyMiddleware(
        thunkMiddleware,
        loggerMiddleware
    )
);

const browserHistory = useRouterHistory(createHistory)({ basename: '/' });

class AppComponent extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Router history={browserHistory}>
                    <Route path="/" component={Site}>
                        <IndexRoute component={NextUpContainer}/>
                        <Route path="/nextup" component={NextUpContainer}/>
                    </Route>
                </Router>
            </Provider>
        );
    }
}

export default AppComponent;
