import React, {Component} from 'react';
import Clock from '../clock/clock';
import {Col,Jumbotron} from 'react-bootstrap';

class Race extends Component {
    render() {
        return (
            <Col xs={12} md={4} className="race">
                <Jumbotron>
                    <h4>{this.props.raceNumber} {this.props.raceName}</h4>
                    <div className="clearfix">
                        <span className="race-location pull-left">{this.props.meeting.meetingName} ({this.props.meeting.location})</span>
                        <span className="race-distance pull-right">{this.props.raceDistance} metres</span>
                    </div>
                    <div className="clearfix">
                        <span className="jump-time pull-left">{this.props.raceStartTime.format('h:mma')}</span>
                        <div className="pull-right">
                            <Clock time={this.props.timeToJump} />
                        </div>
                    </div>
                </Jumbotron>
            </Col>
        );
    }
}

export default Race;
