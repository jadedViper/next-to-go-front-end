import React, {Component} from 'react';
import * as Constants from '../../store/data/constants';

class Clock extends Component {
    componentWillMount() {
        this.hours = 0;
        this.minutes = 0;
        this.seconds = 0;
    }

    componenentDidMount() {
        this.calculateTimeParts();
    }

    componentWillUpdate(nextProps) {
        if(nextProps.time !== this.props.time) {
            this.calculateTimeParts()
        }
    }

    calculateTimeParts() {
        this.hours = Math.floor(this.props.time / Constants.HOUR);
        this.minutes = Math.floor((this.props.time-this.hours * Constants.HOUR) / 60);
        this.seconds = this.props.time % 60;
    }

    padLeft(value) {
        if(value.toString().length < 2 ) {
            return '0'+value;
        }
        return value;
    }

    render() {
        return (
            <div className="ticker">
                <span className="tick">
                    {this.padLeft(this.hours)}
                </span>
                <span className="tick">
                    {this.padLeft(this.minutes)}
                </span>
                <span className="tick">
                    {this.padLeft(this.seconds)}
                </span>
            </div>
        );
    }
}

export default Clock

