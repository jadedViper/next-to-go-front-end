import {connect} from 'react-redux';
import NextUp from './nextup';
import {getNextUp, updateRaces} from '../../store/actions/races';

const mapStateToProps = (state) => {
    return {
        loading: state.raceReducer.loading,
        races: state.raceReducer.races
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getNextUp: (jurisdiction) => {
            dispatch(getNextUp(jurisdiction));
        },
        updateRaces: () => {
            dispatch(updateRaces());
        }
    }
}

const NextUpContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(NextUp)

export default NextUpContainer;
