import React, {Component} from 'react';
import RaceSection from '../racesection/racesection';
import {Grid} from 'react-bootstrap';
import _ from 'lodash';

class NextUp extends Component {
    componentDidMount() {
        this.props.getNextUp('NSW');
        this.interval = setInterval(this.props.updateRaces, 1000);
    }

    componentWillUpdate(nextProps) {
        if (!nextProps.loading && (nextProps.races.racing.length < 3 || nextProps.races.harness.length < 3 || nextProps.races.greyhound.length < 3)) {
            this.props.getNextUp('NSW');
        }
    }

    componponentWillUnmount() {
        clearInterval(this.interval);
    }

    getRaceSections() {
        return _.map(this.props.races, function(section, index) {
            return <RaceSection key={index} title={index} races={section}></RaceSection>
        });
    }

    render() {
        return (
            <Grid>
                {this.getRaceSections()}
            </Grid>
        );
    }
}

export default NextUp;
