import React, {Component} from 'react';
import Race from '../race/race';
import {Row} from 'react-bootstrap';
import _ from 'lodash';

class RaceSection extends Component {
    getRaces(races) {
        return _.map(races, function(race, index) {
            return <Race key={index} {...race} />
        })
    }

    render() {
        return (
            <div className={'race-section '+this.props.title}>
                <div className="header">
                    <h2>{this.props.title}</h2>
                </div>
                <Row>
                    {this.getRaces(this.props.races)}
                </Row>
            </div>
        );
    }
}

export default RaceSection;
