import {combineReducers} from 'redux';
import raceReducer from './reducers/racereducer';

const appStore = combineReducers({
    raceReducer
});

export default appStore;
