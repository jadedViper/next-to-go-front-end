export const API_URL = 'http://revolutionsmultimedia.com.au/nexttogoproxy/index.php';
export const API_REQUEST_NEXTUP = 'api_request_nextup';
export const SET_ERROR = 'set_error';
export const UPDATE_RACES = 'update_races';
export const SET_LOADING = 'set_loading';
export const HOUR = 3600;