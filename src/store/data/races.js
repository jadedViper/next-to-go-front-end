export const racesInitialState = {
    loading: false,
    races: {
        racing: [],
        harness: [],
        greyhound: []
    }
};
