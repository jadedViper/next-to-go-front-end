import axios from 'axios';
import * as Constants from '../data/constants';

export const setNextUp = (races) => {
    return {
        type: Constants.API_REQUEST_NEXTUP,
        races: races
    }
}

export const setError = (message) => {
    return {
        type: Constants.SET_ERROR,
        message: message
    }
}

export const updateRaces = () => {
    return {
        type: Constants.UPDATE_RACES
    }
}

export const setLoading = (status) => {
    return {
        type: Constants.SET_LOADING,
        status: status
    }
}

export const getNextUp = (jurisdiction) => {
    return function(dispatch) {
        dispatch(setLoading(true));
        return axios.get(Constants.API_URL+'?action=getRaces&jurisdiction='+jurisdiction)
            .then(response => response.data)
            .then(json => {
                if(json.result) {
                    dispatch(setLoading(false));
                    dispatch(setNextUp(json.data.races));
                } else {
                    dispatch(setError(json.message));
                }
            })
            .catch(error => {
                dispatch(setLoading(false));
                dispatch(setError(error.message));
            });
    }
}
