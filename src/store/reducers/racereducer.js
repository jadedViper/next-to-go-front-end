import {racesInitialState} from '../data/races';
import _ from 'lodash';
import moment from 'moment';
import * as Constants from '../data/constants';

const RaceReducer = (state = racesInitialState, action) => {
    switch (action.type) {
        case Constants.API_REQUEST_NEXTUP:
            var now = moment();
            var groupedRaces = _.chain(action.races)
                .map(function(item) {
                    item.raceStartTime = moment(item.raceStartTime);
                    item.timeToJump = item.raceStartTime.format('X')-now.format('X');
                    return item;
                })
                .filter(function(item) {
                    return item.timeToJump >= 0;
                })
                .sortBy(['raceStartTime'])
                .groupBy(function(item) {
                    return item.meeting.raceType;
                })
                .value();

            return Object.assign(
                {},
                state,
                {
                    loading: state.loading,
                    races: {
                        racing: _.slice(groupedRaces.R, 0, 3),
                        harness: _.slice(groupedRaces.H, 0, 3),
                        greyhound: _.slice(groupedRaces.G, 0, 3)
                    }
                }
            );
        case Constants.UPDATE_RACES:
            var now = moment();
            var races = _.cloneDeep(state.races);
            var now = moment();
            _.forEach(races, function(items, idx) {
                races[idx] = _.chain(items)
                    .map(function(item) {
                        item.timeToJump = item.raceStartTime.format('X')-now.format('X');
                        return item;
                    })
                    .filter(function(item) {
                        return item.timeToJump >= 0;
                    })
                    .value();
            });

            return {loading: state.loading, races: races}
        case Constants.SET_LOADING:
            return Object.assign(
                {},
                state,
                {
                    loading: action.status
                }
            );
        case Constants.SET_ERROR:
            return state;
        default:
            return state;
    }
}

export default RaceReducer;
