import React, {Component} from 'react';
import Header from '../../components/header/header';

class Site extends Component {
    render() {
        return (
            <div className="app">
                <Header />
                <div className="container-fluid">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default Site;