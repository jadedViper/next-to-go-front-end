# README #

This repository is the front end solution to the Neds Technical Test

### What is this repository for? ###

* Neds Technical Test
* Version 1
* Written with React
* Requires the backend proxy 

### Note ###
* As the tabcorp api does not set CORs headers a proxy is required to consume this api. A basic API proxy written in PHP has been provided at:
* [https://bitbucket.org/jadedViper/next-to-go-back-end](https://bitbucket.org/jadedViper/next-to-go-back-end)

### How do I get set up? ###

* Pull repository to local server
* Run npm install to install node modules required by Configuration
* change the proxy url to suit your server in src/stores/data/constants change API_LOGIN to the uri where the proxy project was installed
* on command line run "npm run serve" to run in development mode
* on command line run "npm run dist" to compile final distribution files.  Distribution files are compiled to the /dist folder

### Where is the solution? ###

* The solution is in the src folder. this solution uses React with Redux.  
* The Redux Store (reducers, actions, constants) is in the  src/store directory
* The components for the page are in the src/components directory
* src/config contains webpack configurations for various development and build environments
* src/containers contains the basic site layout
* src/components/Main.js is the real entry point which uses react-router and react-redux
* src/nextup/nextupcontainer.js connects the redux store with the nextup component
* src/nextup/nextup.js is the base component for the test which uses three sub components:
  * src/racesection/racesection.js
  * src/race/race.js
  * src/clock/clock.js